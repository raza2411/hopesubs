<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hopesubs');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{+<g8WC!6>-^a}sQy|ckt.vmjF;D-Q$G@.gAuR*khi1KiXHc=xpA{g8[U5u.[H1x');
define('SECURE_AUTH_KEY',  'Z+,cBFsMj1n]N65UYGvI)n[$b_=;%A=[W,B`0>6=Qk(XyT/?ybun%8X]^luv5BOJ');
define('LOGGED_IN_KEY',    'zj3#H<,7:U+B:H)vLY[VXJ~]k||M|L?cDBuTH<kz1@yXs+*I0[*lrK1YJvXPJY_y');
define('NONCE_KEY',        '9FVndE8dG.6]Sw,M7dnR3cpolol~>I=3Xjb>tT5z>6U9bYy/3O.^RruD2VuB(H8R');
define('AUTH_SALT',        'd>2n0DB>4i?mPT sLmpiEMr19D$MKu3CuzViVd?; UKN{Fw1+{m=R/:j.LKmfqed');
define('SECURE_AUTH_SALT', '5zBf^{1x5x}(q*@}njc1s6vbJJWE$/rNz84eA<2-!3);4UYW]jB:q^ t[c;d.qp0');
define('LOGGED_IN_SALT',   '<tWx&X,H Rh~k:z5ZU5;8Xkg*IxIG0e]dsbX!J-~%R,P{C2x<8 `P:I$o1u+iX$S');
define('NONCE_SALT',       'XG]=#l|=[jg^4NnUwT>S*9ey(7#P%(+!/-.MkF 1VQacClIZe_S;l%+(pM;dm;}6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define( 'CONCATENATE_SCRIPTS', false );
