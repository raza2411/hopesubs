/*global $, alert, console*/

$(function () {





    $(window).on("scroll", function () {

        if ($(window).scrollTop()) {

            $('nav').addClass('black');

        }

        else {

            $('nav').removeClass('black');

        }

    });







    // Adjusting slider's height & resize

    $('.slider, .slider img').height($(window).height());



    $(window).resize(function () {

        $('.slider, .slider img').height($(window).height());

    });



    $('.carousel-item .post').height(0.5 * $(window).height());

    $('.carousel-item .post').width(.2 * $(window).width());

    $(window).resize(function () {

        $('.carousel-item .post').height(0.5 * $(window).height());

        $('.carousel-item .post').width(0.2 * $(window).width());

    });





    $('.responsive, .autoplay').slick({

        rtl: true,

        dots: true,

        infinite: false,

        speed: 500,

        slidesToShow: 4,

        slidesToScroll: 1,

        autoplay: true,

        autoplaySpeed: 1500,

        responsive: [

            {

                breakpoint: 992,

                settings: {

                    slidesToShow: 3,

                    slidesToScroll: 1,

                    infinite: false,

                    dots: true,

                    rtl: true,

                }

            },

            {

                breakpoint: 768,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1,

                    infinite: false,

                    dots: true,

                    rtl: true,

                }

            },

            {

                breakpoint: 576,

                settings: {

                    slidesToShow: 1,

                    slidesToScroll: 1,

                    infinite: false,

                    dots: true,

                    rtl: true,

                }

            }

            // You can unslick at a given breakpoint now by adding:

            // settings: "unslick"

            // instead of a settings object

        ]

    });

    // info - espides toggle 

    $('#episodes').click(function () {

        $('.titles, .info-value').hide();

        $('.episodes, .episodes-icons').show();

    });

    $('#info').click(function () {

        $('.titles, .info-value').show();

        $('.episodes, .episodes-icons').hide();

    });



    $('.navbar-toggler').on('click', function () {

        if ($(this).on()) {

            $('nav').toggleClass('black');

        }

    });

    // Dropdown toggle
$('.dropdown').click(function(){
  $(this).next('.sub-menu').toggle();
});
 
$(document).click(function(e) {
  var target = e.target;
  if (!$(target).is('.dropdown') && !$(target).parents().is('.dropdown')) {
    $('.sub-menu').hide();
  }
});

    if ($(window).width() < 768) {

        $('.drama-page .drama-info').width(17 + $('.drama-page .story').width());

    }

    

    $(window).resize(function () {

        if ($(window).width() < 768) {

            $('.drama-page .drama-info').width(17 + $('.drama-page .story').width());

        }

    });



    if ($(window).width() < 768) {

        $('.watching-page .drama-info').width( $('.watching-page .embed-responsive-item').width() - 29);

    }

    

    $(window).resize(function () {

        if ($(window).width() < 768) {

            $('.watching-page .drama-info').width( $('.watching-page .embed-responsive-item').width() - 29);

        }

    });

});