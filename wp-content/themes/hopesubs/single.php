<?php
/**
* The template for displaying all single posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
get_header();
?>
<?php if (is_active_sidebar('episode_sidebar')) : ?>
<?php dynamic_sidebar('episode_sidebar'); ?>
<?php endif; ?>
<?php
// Start the loop.
while (have_posts()) : the_post();
?>



<?php
$episode = $_GET['epi'];
if ($episode == '1') {
?>
<div class="watching-page">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <?php
                $custom_title = get_post_meta(get_the_ID(), 'ms_custom_title', true);
                if ($custom_title != '') {
                ?>
                <h1 class="name"><?php echo $custom_title; ?></h1>
                <?php } ?>
            </div>
            <div class="col-12">
                <div class="row mb-5 justify-content-center">
                    <?php 
                    if( have_rows('episode') ):
                            $counter = 1;

                            // loop through the rows of data
                            while ( have_rows('episode') ) : the_row();

                    	$download_button = get_sub_field ('download-page');

                    
					
                    if($counter == 1){
                    ?>

	                    <a class="col-5 col-md-4 col-lg-2 button btn-down" id="btn-download" href="<?php echo $download_button; ?>" target="blank">تحميل
	                    </a>

					<?php
					}
					$counter++;
                    	endwhile;
                        	endif;

                    ?>




                    <div class="btn-group col-5 col-md-4 col-lg-2 more-servers">
                        <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            مزيد من السيرفرات
                        </button>

                        <div class="dropdown-menu">

                            <?php
                            if( have_rows('episode') ):
                            $episodecounter = 1;

                            // loop through the rows of data
                            while ( have_rows('episode') ) : the_row();

                            // display a sub field value
                            ?>
                            <ul data-episode = "<?php echo $episodecounter; ?>" style = " <?php echo ($episodecounter!=1)?'display:none':''; ?>">
                                <?php $servers = get_sub_field('server_name');
                                $serverCounter = 0;
                                $links = get_sub_field('server_link');
                                foreach($servers as $server):
                                ?>
                                <a href="#" class="dropdown-item" data-link="<?php echo $links[$serverCounter]['server_link']; ?>"><?php echo $server['server_name']; ?></a>
                                <?php
                                $serverCounter++;
                                endforeach; ?>
                            </ul>
                            <?php 
                            $episodecounter++;
                            endwhile;
                            endif;

                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-lg-3 drama-info">
                <div class="row">
                    <span class="button col">الحلقات</span>
                </div>
                <div class="episodes col" id="test-list">
                    <div class="episode list">
                        <?php
                        if( have_rows('episode') ):
                        $leftcounter = 1;
                        // loop through the rows of data
                        while ( have_rows('episode') ) : the_row();

                        // display a sub field value
                        $name = get_sub_field('episode_name');
                        $link = get_sub_field('episode_link');
                        $download_button = get_sub_field ('download-page');
                        //print_r($name);
                        //print_r($link);

                        $ids = explode('/', $link);
                        ?>
                        <ul class="name">

                            <li class="watching" data-download="<?php echo $download_button; ?>" data-episode = "<?php echo $leftcounter; ?>" data-link_id="<?php echo strbefore($ids['4'], '?'); ?>" data-link="<?php echo $link; ?>">
                                <span><?php echo $name; ?></span>
                                <a href="#">
                                    <i class="far fa-play-circle"></i>
                                </a>
                            </li>

                        </ul>

                        <?php 
                        $leftcounter++;
                        endwhile;
                        endif;
                        ?>
                    </div>
                    <ul class="page_episode_panel_tabs col pagination mb5 justify-content-center">
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-lg-9">
                <?php $first_episode = get_field('episode'); ?>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="video-ep" class="embed-responsive-item" src="<?php echo $first_episode[0]['episode_link'];?>" allowfullscreen></iframe>  
                </div> 
            </div>               
        </div>
        <div class="row row mb-5 justify-content-center">
        	<div class="col-lg-8">
        		<div id="disqus_thread"></div>
        		<script>
					(function() { // DON'T EDIT BELOW THIS LINE
						var d = document, s = d.createElement('script');
						s.src = 'https://hope-6.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
					})();
				</script>
				<noscript>Please enable JavaScript to view the <a href="#disqus_thread">comments powered by Disqus.</a></noscript>
        	</div>
        </div>
    </div>
</div>


<!-- Drama Detail page -->


<?php } elseif ($episode == '2') {
?>
<div class="drama-page">
    <div class="container">
        <div class="row text-center">
            <?php
            $text_medium = get_post_meta(get_the_ID(), 'ms_heading', true);
            $textarea = get_post_meta(get_the_ID(), 'ms_detail', true);
            $name = get_post_meta(get_the_ID(), 'ms_series-name', true);
            $bel = get_post_meta(get_the_ID(), 'ms_bel-name', true);
            $known = get_post_meta(get_the_ID(), 'ms_also-known', true);
            $number = get_post_meta(get_the_ID(), 'ms_ring-number', true);
            $country = get_post_meta(get_the_ID(), 'ms_country', true);
            $broadcast = get_post_meta(get_the_ID(), 'ms_broadcast', true);
            ?>
            <div class="col-md-5 col-lg-4 col-xl-3 poster">
                <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="#">
            </div>
            <div class="col-md-7 col-lg-8 col-xl-6 sto">
                <p class="name h1"><?php the_title(); ?></p>
                <div class="story">
                    <p class="h2"> <?php echo $text_medium; ?></p>
                    <p><?php echo $textarea; ?></p>
                    <a href="<?php echo $host[1]; ?>?epi=1">
                        <button class="button">مشاهدة الان</button>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 drama-info">
                <div class="row">
                    <button class="button col-6" id="info">معلومات</button>
                    <button class="button col-6" id="episodes">الحلقات</button>
                </div>
                <div class="info row">
                    <div class="titles col-5">
                        <span>اسم المسلسل</span>
                        <span>الاسم بالعربى</span>
                        <span>يعرف ايضا بـ</span>
                        <span>عدد الحلقات</span>
                        <span>البلد المنتج</span>
                        <span>موعد البث</span>
                    </div>
                    <div class="info-value col-7">
                        <span><?php echo $name; ?></span>
                        <span><?php echo $bel; ?></span>
                        <span><?php echo $known; ?></span>
                        <span><?php echo $number; ?></span>
                        <span><?php echo $country; ?></span>
                        <span><?php echo $broadcast; ?></span>
                    </div>
                    <div class="episodes col">
                        <div class="episode">
                            <ul>
                                <?php
                                $post_id = get_the_ID();
                                $meta = get_post_meta($post_id, 'ms_custom_episodes', true);
                                // echo '<pre>'; var_dump($meta); exit;
                                if ($meta) {
                                foreach ($meta as $met) {
                                ?>
                                <li class="episode" data-link="<?php echo $met['ms_episode_link']; ?>">
                                    <span>
                                        <?php echo $met['ms_episode_name']; ?>
                                    </span>
                                    <a href="<?php echo $host[1]; ?>?epi=1">
                                        <i class="far fa-play-circle"></i>
                                    </a>
                                </li>
                                <?php
                                	}
                                }
                                ?>
                            </ul>
                        </div>
                        <ul class="page_episode_panel_tabs col">
                            <li>
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
          <div class="row row mb-5 justify-content-center">
        	<div class="col-lg-8">
        		<div id="disqus_thread"></div>
        		<script>
					(function() { // DON'T EDIT BELOW THIS LINE
						var d = document, s = d.createElement('script');
						s.src = 'https://hope-6.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
					})();
				</script>
				<noscript>Please enable JavaScript to view the <a href="#disqus_thread">comments powered by Disqus.</a></noscript>
        	</div>
        </div>
    </div>
</div>
<?php } else {

}
?>

<?php
endwhile;
?>
<script>
    jQuery(document).ready(function ($) {
        $('.watching').click(function (e) {
            e.preventDefault();
            var link = $(this).attr('data-link');
            var id = $(this).data('link_id');
            $('#video-ep').attr('src', link);
            $('#videoid').val(id);
            var $download = $(this).data('download');
            $('#btn-download').attr('href',$download);
            var $episode = $(this).data('episode');
            $('.dropdown-menu ul').hide();
            $('.dropdown-menu ul').each(function () {
                if ($(this).data('episode') == $episode) {
                    $(this).show();
                }
            });
        });

        $('.dropdown-item').click(function () {
            var link = $(this).data('link');
            $('#video-ep').attr('src', link);
        });

        

    });
</script>



<?php
get_footer();
