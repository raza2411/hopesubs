<?php
/**
 * Template Name: About-us Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<div class="about-us">
    <div class="container">
        <div class="col">
            <div class="title">
                <?php the_title(); ?>   
            </div>
            
                <?php while (have_posts()) : the_post(); ?> 
                    <p class="about-us-p">
                        <?php the_content(); ?> 
                    </p>
                <?php
                    endwhile;
                    wp_reset_query();
                ?>
        </div>
        
          
        <div class="col">
            <div class="title">فريقنا</div>
            <div class="our-team row text-center">
                
                <?php
                    $args = array(
                        'post_type' => 'about_us',
                        'order' => 'ASC',
                        'posts_per_page' => -1
                    );
            $loop = new WP_Query($args);
            while ($loop->have_posts()) : $loop->the_post();
                
            $text_small = get_post_meta(get_the_ID(),'ms_designation',true);
            $text = get_post_meta(get_the_ID(),'ms_text',true);
            $facebook = get_post_meta(get_the_ID(),'ms_facebook',true);
             {
                ?>
                <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                    <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" />
                    <span><?php the_title();?></span>
                    <span><?php echo $text_small; ?></span>
                    <p><?php echo $text; ?></p>
                    <div class="icons">
                        <a href="<?php echo facebook; ?>">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fas fa-envelope"></i>
                        </a>
                    </div>
                </div>
                   
                <?php 
            }
            endwhile;
            ?>
                
            </div>
        </div>
         
    </div>
</div>
<?php get_footer(); ?>