<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="videos-page">
    <div class="container">
        <div class="title col"><?php echo single_cat_title( '', false ); ?></div>
        <div class="drama">
            <div class="row text-center">
                                    <?php
            while ( have_posts() ) : the_post();
            
                ?>
            <?php //the_id(); ?>    
                 <div class="col-xl-3 col-sm-6 col-lg-4">
                            <div class="content">
                                <a href="<?php the_permalink(); ?>?epi=2">
                                    <div class="content-overlay"></div>
                                    <img class="content-image" src="<?php the_post_thumbnail_url(); ?>" alt="#">
                                    <div class="content-details fadeIn-bottom">
                                        <h3 class="content-title"><?php the_title(); ?></h3>
                                        <p class="content-text">
                                            <?php the_excerpt(); ?>
                                        </p>
                                        <a class="watch_now" href="<?php the_permalink(); ?>?epi=1"><span>شاهد الان</span></a>
                                    </div>

                                </a>
                            </div>
                        </div>
                
                 <?php 
            
            endwhile;
            ?>
               
            </div>
        </div>
    </div>
</div>

<?php get_footer();
