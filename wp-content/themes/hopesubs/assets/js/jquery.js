/*global jQuery, alert, console*/
jQuery(function () {


    jQuery(window).on("scroll", function () {
        if (jQuery(window).scrollTop()) {
            jQuery('nav').addClass('black');
        } else {
            jQuery('nav').removeClass('black');
        }
    });



    // Adjusting slider's height & resize
    jQuery('.slider, .slider img').height(jQuery(window).height());

    jQuery(window).resize(function () {
        jQuery('.slider, .slider img').height(jQuery(window).height());
    });

    jQuery('.carousel-item .post').height(0.5 * jQuery(window).height());
    jQuery('.carousel-item .post').width(.2 * jQuery(window).width());
    jQuery(window).resize(function () {
        jQuery('.carousel-item .post').height(0.5 * jQuery(window).height());
        jQuery('.carousel-item .post').width(0.2 * jQuery(window).width());
    });


    jQuery('.responsive, .autoplay').slick({
        rtl: true,
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true,
                    rtl: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true,
                    rtl: true,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true,
                    rtl: true,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    // info - espides toggle 
    jQuery('#episodes').click(function () {
        jQuery('.titles, .info-value').hide();
        jQuery('.episodes, .episodes-icons').show();
    });
    jQuery('#info').click(function () {
        jQuery('.titles, .info-value').show();
        jQuery('.episodes, .episodes-icons').hide();
    });


    jQuery(document).ready(function () {
        jQuery(document).click(function () {
            jQuery('.sub-menu').hide();
        });
        jQuery(".nav-item a").click(function (e) {
            e.stopPropagation();
            jQuery('.sub-menu').not(jQuery(this).next('.sub-menu')).hide();
            jQuery(this).next('.sub-menu').toggle();

        });

    });


    if (jQuery(window).width() < 768) {
        jQuery('.drama-page .drama-info').width(17 + jQuery('.drama-page .story').width());
    }

    jQuery(window).resize(function () {
        if (jQuery(window).width() < 768) {
            jQuery('.drama-page .drama-info').width(17 + jQuery('.drama-page .story').width());
        }
    });

    if (jQuery(window).width() < 768) {
        jQuery('.watching-page .drama-info').width(jQuery('.watching-page .embed-responsive-item').width() - 29);
    }

    jQuery(window).resize(function () {
        if (jQuery(window).width() < 768) {
            jQuery('.watching-page .drama-info').width(jQuery('.watching-page .embed-responsive-item').width() - 29);
        }
    });
});
jQuery("#btn-download").click(function (e) {
//    e.preventDefault();
    jQuery("#type").trigger("click");
});

jQuery(window).on('load', function () {
    setTimeout(function () {
        var link = $('#custom-download #my-download li:nth-child(3) a').attr('href');
        if (link) {
            window.location.href = link;
        }
    }, 0);
});

