<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <title>Hopesubs</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/assets/images/logo-header.png">
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
              crossorigin="anonymous">
        <!-- Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
              crossorigin="anonymous"> 
        <link href="https://fonts.googleapis.com/css?family=Cairo:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/main.css">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>


        <header>
            <div class="header">
                <div class="container">
                    <nav class="navbar fixed-top navbar-expand-lg navbar-light ">
                        <div class="container">
                            <a href="<?php bloginfo('url'); ?>"> <img src="<?php echo get_option_tree('logo_image', '', false); ?>" alt="#" /></a>
                            <button class="navbar-toggler nav-btn" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span>
                                    <i class="fas fa-bars fa-2x"></i>
                                </span>
                            </button>
                            <div class="collapse navbar-collapse links justify-content-center " id="navbarNavDropdown">
                                <?php
                        wp_nav_menu(array('menu' => 'Main Menu', 'container' => '', 'container_class' => '', 'container_id' => '', 'menu_class' => 'navbar-nav drop',
                            'menu_id' => '',
                        ));
                        ?>
                            </div>
                           <?php if ( is_user_logged_in() ) { ?>
							<div class="log-out">
                                <a href="<?php echo wp_logout_url(); ?>">الخروج</a>
                            </div>
							<?php } ?>        
                        </div>
                    </nav>
                </div>
            </div>
        </header>
