<?php
/**
 * Template Name: Download Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<div id="download-button">
    <div class="container">
        <div class="title col"><?php the_title(); ?></div>
        <div class="row">

                                    <!-- Medium Quality -->

                <?php
				if( have_rows('medium_quality') ):
					?>
					<div class="col-md-4 centered">
                    
                		<span class="quality">Medium Quality</span>
					<?php
					// loop through the rows of data
					while ( have_rows('medium_quality') ) : the_row();
				?>
				
				<?php
						// display a sub field value
						//the_sub_field('sub_field_name');
				?>
                    <?php
                    	$site_link = get_sub_field('site_link');
                    	$site_name = get_sub_field('site_name');
                    ?>

                        <a class="button btn-down my-button" id="btn-download" href="<?php echo $site_link; ?>" target="blank">تحميل
                            <span><?php echo $site_name; ?></span>
                        </a>
				<?php
					endwhile;
					?>
					</div><?php
				endif;

				?>

                                        <!-- High Quality -->

                <?php
                if( have_rows('high_quality') ):                    
                    ?>
                    <div class="col-md-4 centered">
                    
                        <span class="quality">High Quality</span>
                    <?php
                    // loop through the rows of data
                    while ( have_rows('high_quality') ) : the_row();
                ?>
                
                <?php
                        // display a sub field value
                        //the_sub_field('sub_field_name');
                ?>
                    <?php
                        $site_link = get_sub_field('site_link');
                        $site_name = get_sub_field('site_name');
                    ?>

                     <a class="button btn-down my-button" id="btn-download" href="<?php echo $site_link; ?>" target="blank">تحميل
                            <span><?php echo $site_name; ?></span>
                        </a>
                <?php
                    endwhile;
                    ?>
                    </div><?php
                endif;

                ?>


                                                <!-- Full High Quality -->

                <?php
                if( have_rows('full_high_quality') ):
                    ?>
                    <div class="col-md-4 centered">
                    
                        <span class="quality">Full High Quality</span>
                    <?php
                    // loop through the rows of data
                    while ( have_rows('full_high_quality') ) : the_row();
                ?>
                
                <?php
                        // display a sub field value
                        //the_sub_field('sub_field_name');
                ?>
                    <?php
                        $site_link = get_sub_field('site_link');
                        $site_name = get_sub_field('site_name');
                    ?>

                        <a class="button btn-down my-button" id="btn-download" href="<?php echo $site_link; ?>" target="blank">تحميل
                            <span><?php echo $site_name; ?></span>
                        </a>
                <?php
                    endwhile;
                    ?>
                    </div><?php
                endif;

                ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>