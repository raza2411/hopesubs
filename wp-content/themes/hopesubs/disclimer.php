<?php
/**
 * Template Name: Desclimer Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<div class="desclimer">
    <div class="container">
        <div class="title">
            <?php the_title(); ?>
        </div>
        <?php while (have_posts()) : the_post(); ?> 
            <p class="about-us-p">
                <?php the_content(); ?> 
            </p>
            <?php
                endwhile;
            wp_reset_query();
            ?>
    </div>
</div>
<?php get_footer(); ?>