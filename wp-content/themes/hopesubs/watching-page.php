<?php
/**
 * Template Name: Watching-page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<div class="watching-page">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h1 class="name">مشاهدة مسلسل العودة الحلقة 15 كاملة </h1>
            </div>
            <div class="col-12">
                <div class="row mb-5 justify-content-center">
                    <button class="col-5 col-md-4 col-lg-2 button btn-down">تحميل</button>
                    <div class="btn-group col-5 col-md-4 col-lg-2 more-servers">
                        <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            مزيد من السيرفرات
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">openload</a>
                            <a class="dropdown-item" href="#">openload</a>
                            <a class="dropdown-item" href="#">openload</a>
                            <a class="dropdown-item" href="#">openload</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 drama-info">
                <div class="row">
                    <span class="button col">الحلقات</span>
                </div>
                <div class="episodes col">
                    <div class="episode">
                        <span>الحلقة الاولى</span>
                        <a href="#">
                            <i class="far fa-play-circle"></i>
                        </a>
                    </div>
                    <div class="episode">
                        <span>الحلقة الاولى</span>
                        <a href="#">
                            <i class="far fa-play-circle"></i>
                        </a>
                    </div>
                    <div class="episode">
                        <span>الحلقة الاولى</span>
                        <a href="#">
                            <i class="far fa-play-circle"></i>
                        </a>
                    </div>
                    <div class="episode">
                        <span>الحلقة الاولى</span>
                        <a href="#">
                            <i class="far fa-play-circle"></i>
                        </a>
                    </div>
                    <div class="episode">
                        <span>الحلقة الاولى</span>
                        <a href="#">
                            <i class="far fa-play-circle"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>