<?php
/**
 * Template Name: Home Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>



<div class="home-page">
    <div id="carouselExampleIndicators" class="carousel slide slider" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            $args = array(
                'post_type' => 'slider',
                'order' => 'ASC',
                'posts_per_page' => -1
            );
            $loop = new WP_Query($args);
            $counter = 0;
            while ($loop->have_posts()) : $loop->the_post();
                if ($counter == 0) {
                    $act = "active";
                } else {
                    $act = "";
                }
                ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $counter; ?>" class="<?php echo $act; ?>"></li>
                <?php
                $counter++;
            endwhile;
            ?>
        </ol>

        <div class="carousel-inner text-center" role="listbox">
            <?php
            $args = array(
                'post_type' => 'slider', 'drama_category',
                'order' => 'ASC',
                'posts_per_page' => -1
            );
            $loop = new WP_Query($args);
            $counter = 1;
            while ($loop->have_posts()) : $loop->the_post();
                if ($counter == 1) {
                    $act = "active";
                } else {
                    $act = "";
                }
                ?>
                <div class="carousel-item <?php echo $act; ?>">
                    <img class="d-block img-fluid" src="<?php the_post_thumbnail_url(); ?>">
                    <div class="carousel-caption d-none d-block row">
                        <h1 class="col-12"><?php the_title(); ?></h1>
                        <h2 class="col-12"><?php the_content(); ?></h2>
                        <?php if ($counter == 1) { ?>

                               <?php
                               if ( is_user_logged_in() ) { ?>
                                 <?php   } else {
                                        ?>
                            <!-- Button trigger modal log in-->
                            <button type="button" class="button btn col-md-3 col-5" data-toggle="modal" data-target="#logIn">
                                تسجيل دخول
                            </button>
                            <!-- Button trigger modal sign up-->
                            <button type="button" class="button btn col-md-3 col-5" data-toggle="modal" data-target="#signUp">
                                عضوية جديدة
                            </button>
                            <?php
                            }
                            ?>

                        <?php } ?>
                       
                        <?php
                            $slider_url_1 = get_post_meta(get_the_ID(), 'ms_slider-url-1', true);
                        ?>
                        <a href="<?php echo $slider_url_1; ?>">
                            <?php
                            $slider1 = get_post_meta(get_the_ID(), 'ms_slider1', true);
                             
                            if ($slider1 != '') {
                                ?>
                                <img class="post col" src="<?php echo $slider1; ?>">
                            <?php } ?>
                        </a>


                        <?php
                            $slider_url_2 = get_post_meta(get_the_ID(), 'ms_slider-url-2', true);
                        ?>
                        <a href="<?php echo $slider_url_2; ?>">
                            <?php
                            $slider2 = get_post_meta(get_the_ID(), 'ms_slider2', true);
                           
                            if ($slider1 != '') {
                                ?>
                                <img class="post col" src="<?php echo $slider2; ?>">
                            <?php } ?>
                        </a>
                    </div>
                </div>
                 <!-- Modal log in -->
            <div class="modal fade" id="logIn" tabindex="-1" role="dialog" aria-labelledby="logInTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="logInTitle">تسجيل الدخول</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                               <?php echo do_shortcode('[profilepress-login id="1"]'); ?>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal sign up -->
            <div class="modal fade" id="signUp" tabindex="-1" role="dialog" aria-labelledby="signUpTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="signUpTitle">عضوية جديدة</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           
                                <?php echo do_shortcode('[profilepress-registration id="1"]'); ?>
                           
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                        </div>
                    </div>
                </div>
            </div>
                <?php
                $counter++;
            endwhile;
            ?>

        </div>
    </div>
    <div class="container">
        <div class="title col">أفضل الدرامات</div>
        <div class="drama">
            <div class="row text-center">
                <?php
                $args = array(
                    'post_type' => 'drama_category',
                    'order' => 'DESC',
                    'posts_per_page' => 8
                );
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();

                    $check_box = get_post_meta(get_the_ID(), 'ms_first_img', true);
                    if ($check_box == 'on') {
                        ?>

                        <div class="col-xl-3 col-sm-6 col-lg-4">
                            <div class="content">
                                <a href="<?php the_permalink(); ?>?epi=2">
                                    <div class="content-overlay"></div>
                                    <img class="content-image" src="<?php the_post_thumbnail_url(); ?>" alt="#">
                                    <div class="content-details fadeIn-bottom">
                                        <h3 class="content-title"><?php the_title(); ?></h3>
                                        <p class="content-text">
                                            <?php the_excerpt(); ?>
                                        </p>
                                        <a class="watch_now" href="<?php the_permalink(); ?>?epi=1"><span>شاهد الان</span></a>
                                    </div>

                                </a>
                            </div>
                        </div>

                        <?php
                    }
                endwhile;
                ?>

                <div class="col-xs-12 col-lg-12">
                   <a href="<?php bloginfo('url'); ?>/all-drama/"><button class="button-more">المزيد ... </button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="new-episodes">
        <div class="container">
            <div class="title col">الحلقات الجديدة</div>
            <div class="responsive autoplay text-center">

                <?php
                global $wpdb;
                $postID = get_the_ID();
                $query = "SELECT * FROM wp_postmeta WHERE meta_key = 'ms_custom_episode' LIMIT 10";
                $results = $wpdb->get_results($query);
                foreach ($results as $result):
                    $post = get_post($result->post_id);
//                echo '<pre>';
//                var_dump($post);
//                exit;
    ?>
                
                    <div>
                        <a href="<?php the_permalink(); ?>?epi=2">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="#" />
                            <p><?php echo $post->post_title; ?></p>
                        </a>  
                    </div>
                
                    <?php
                endforeach;
                ?>
            </div>
            <div class="col-xs-12 col-lg-12 text-center">
                <a href="<?php bloginfo('url'); ?>/207-2/"><button class="button-more">المزيد ... </button></a>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary button-more cl-btn" data-toggle="modal" data-target="#calender">
                    اظغط هنا لمعرفة مواعيد الحلقات
                </button>
                <!-- Modal -->
                <div class="modal fade" id="calender" tabindex="-1" role="dialog" aria-labelledby="calender" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <iframe src="https://calendar.google.com/calendar/embed?src=huimanghopy%40gmail.com&ctz=Africa%2FCairo" style="border: 0"
                                        width="100%" height="500px" frameborder="0" scrolling="no"></iframe>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>