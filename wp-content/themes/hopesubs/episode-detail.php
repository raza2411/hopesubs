<?php
/**
 * Template Name: Episode-Detail Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<div class="drama-page">
    <div class="container">
        <div class="row text-center">
            <?php
            $args = array(
                'post_type' => 'drama_category',
                'order' => 'DESC',
                'posts_per_page' => 01, 
            );
            $loop = new WP_Query($args);
            while ($loop->have_posts()) : $loop->the_post();

                $textarea = get_post_meta(get_the_ID(),'ms_detail',true);
                $text_medium = get_post_meta(get_the_ID(),'ms_heading',true);
            ?>

            <div class="col-md-5 col-lg-4 col-xl-3 poster">
                <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="#">
            </div>
            <div class="col-md-7 col-lg-8 col-xl-6 sto">
                <p class="name h1"><?php the_title(); ?></p>
                <div class="story">
                    <p class="h2"> <?php echo $text_medium; ?></p>
                    <p><?php echo $textarea; ?></p>
                    <a href="<?php bloginfo('url'); ?>/drama_category/love-returns/"><button class="button">مشاهدة الان</button></a>
                </div>
            </div>
             
            <div class="col-xl-3 col-lg-4 col-md-5 drama-info">
                <div class="row">
                    <button class="button col-6" id="info">معلومات</button>
                    <button class="button col-6" id="episodes">الحلقات</button>
                </div>
                <div class="info row">
                    <div class="titles col-5">
                        <span>اسم المسلسل</span>
                        <span>الاسم بالعربى</span>
                        <span>يعرف ايضا بـ</span>
                        <span>عدد الحلقات</span>
                        <span>البلد المنتج</span>
                        <span>موعد البث</span>
                    </div>
                    <div class="info-value col-7">
                        <span>That Man Oh Soo</span>
                        <span>ذلك الرجل أوه سو</span>
                        <span>그남자 오수</span>
                        <span>16</span>
                        <span>كوريا الجنوبية</span>
                        <span> 5 مارس 2018 لـ24 أبريل 2018 (الإثنين والثلاثاء)</span>
                    </div>
                    <div class="episodes col">
                        <div class="episode">
                            <span>الحلقة الاولى</span>
                            <a href="#">
                                <i class="far fa-play-circle"></i>
                            </a>
                        </div>
                        <div class="episode">
                            <span>الحلقة الاولى</span>
                            <a href="#">
                                <i class="far fa-play-circle"></i>
                            </a>
                        </div>
                        <div class="episode">
                            <span>الحلقة الاولى</span>
                            <a href="#">
                                <i class="far fa-play-circle"></i>
                            </a>
                        </div>
                        <div class="episode">
                            <span>الحلقة الاولى</span>
                            <a href="#">
                                <i class="far fa-play-circle"></i>
                            </a>
                        </div>
                        <div class="episode">
                            <span>الحلقة الاولى</span>
                            <a href="#">
                                <i class="far fa-play-circle"></i>
                            </a>
                        </div>
                        <ul class="page_episode_panel_tabs col">
                            <li>
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            
            endwhile;
            ?>
    </div>
</div>
<script src="../js/jquery.js"></script>
<?php get_footer(); ?>