<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
?>

<footer>
    <div class="footer-wrapper">
        <div class="row text-center">
            <div class="col-12 col-md-3">
                <a href="<?php bloginfo('url'); ?>">
                    <img src="<?php echo get_option_tree('footer_logo_image', '', false); ?>" alt="#">
                </a>
            </div>
            <div class="col-6 col-sm-6 col-md-3  more-info">
                <?php dynamic_sidebar( 'home_right_1' ); ?>
            </div>
            <div class="col-6 col-sm-6 col-md-3 social">
                <h4>تابعنا على</h4>
                <a href="<?php echo get_option_tree('social_facebook', '', false); ?>" target="_blank">
                    <i class="fab fa-facebook fa-2x"></i>
                </a>
                <a href="<?php echo get_option_tree('social_tweeter', '', false); ?>" target="_blank">
                    <i class="fab fa-twitter fa-2x"></i>
                </a>
                <a href="<?php echo get_option_tree('social_instagram', '', false); ?>" target="_blank">
                    <i class="fab fa-instagram fa-2x"></i>
                </a>
            </div>
            <div class="col col-md-3 developer">
                <p><?php echo get_option_tree('copyright', '', false); ?></p>
            </div>
        </div>
    </div>
</footer>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/propper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script> -->
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>  
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<div id="google_translate_element"></div><script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'ar', includedLanguages: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script>
    $(document).ready(function(){
        $(".forget-password").click(function(){
        $(".detail").hide();
        
    });
    $(".forget-password").click(function(){
        $(".reset-password").show();
    });
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>  
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script>
    var monkeyList = new List('test-list', {
  valueNames: ['name'],
  page: 10,
  pagination: true
});
</script>
<script id="dsq-count-scr" src="//hope-6.disqus.com/count.js" async></script>


<?php wp_footer(); ?>
</body>    
</html>
