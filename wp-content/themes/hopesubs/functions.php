<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */
/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.7-alpha', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
    return;
}

add_filter('ot_show_pages', '__return_false');

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter('ot_theme_mode', '__return_true');
add_filter('the_content', 'do_shortcode');

/**
 * Required: include OptionTree.
 */
include_once( 'option-tree/ot-loader.php' );
/**
 * Theme Options
 */
include_once( 'includes/theme-options.php' );

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function twentyseventeen_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
     * If you're building a theme based on Twenty Seventeen, use a find and replace
     * to change 'twentyseventeen' to the name of your theme in all the template files.
     */
    load_theme_textdomain('twentyseventeen');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    add_image_size('twentyseventeen-featured-image', 2000, 1200, true);

    add_image_size('twentyseventeen-thumbnail-avatar', 100, 100, true);

    // Set the default content width.
    $GLOBALS['content_width'] = 525;

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(array(
        'top' => __('Top Menu', 'twentyseventeen'),
        'social' => __('Social Links Menu', 'twentyseventeen'),
    ));

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ));

    // Add theme support for Custom Logo.
    add_theme_support('custom-logo', array(
        'width' => 250,
        'height' => 250,
        'flex-width' => true,
    ));

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, and column width.
     */
    add_editor_style(array('assets/css/editor-style.css', twentyseventeen_fonts_url()));

    // Define and register starter content to showcase the theme on new sites.
    $starter_content = array(
        'widgets' => array(
            // Place three core-defined widgets in the sidebar area.
            'sidebar-1' => array(
                'text_business_info',
                'search',
                'text_about',
            ),
            // Add the core-defined business info widget to the footer 1 area.
            'sidebar-2' => array(
                'text_business_info',
            ),
            // Put two core-defined widgets in the footer 2 area.
            'sidebar-3' => array(
                'text_about',
                'search',
            ),
        ),
        // Specify the core-defined pages to create and add custom thumbnails to some of them.
        'posts' => array(
            'home',
            'about' => array(
                'thumbnail' => '{{image-sandwich}}',
            ),
            'contact' => array(
                'thumbnail' => '{{image-espresso}}',
            ),
            'blog' => array(
                'thumbnail' => '{{image-coffee}}',
            ),
            'homepage-section' => array(
                'thumbnail' => '{{image-espresso}}',
            ),
        ),
        // Create the custom image attachments used as post thumbnails for pages.
        'attachments' => array(
            'image-espresso' => array(
                'post_title' => _x('Espresso', 'Theme starter content', 'twentyseventeen'),
                'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
            ),
            'image-sandwich' => array(
                'post_title' => _x('Sandwich', 'Theme starter content', 'twentyseventeen'),
                'file' => 'assets/images/sandwich.jpg',
            ),
            'image-coffee' => array(
                'post_title' => _x('Coffee', 'Theme starter content', 'twentyseventeen'),
                'file' => 'assets/images/coffee.jpg',
            ),
        ),
        // Default to a static front page and assign the front and posts pages.
        'options' => array(
            'show_on_front' => 'page',
            'page_on_front' => '{{home}}',
            'page_for_posts' => '{{blog}}',
        ),
        // Set the front page section theme mods to the IDs of the core-registered pages.
        'theme_mods' => array(
            'panel_1' => '{{homepage-section}}',
            'panel_2' => '{{about}}',
            'panel_3' => '{{blog}}',
            'panel_4' => '{{contact}}',
        ),
        // Set up nav menus for each of the two areas registered in the theme.
        'nav_menus' => array(
            // Assign a menu to the "top" location.
            'top' => array(
                'name' => __('Top Menu', 'twentyseventeen'),
                'items' => array(
                    'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
                    'page_about',
                    'page_blog',
                    'page_contact',
                ),
            ),
            // Assign a menu to the "social" location.
            'social' => array(
                'name' => __('Social Links Menu', 'twentyseventeen'),
                'items' => array(
                    'link_yelp',
                    'link_facebook',
                    'link_twitter',
                    'link_instagram',
                    'link_email',
                ),
            ),
        ),
    );

    /**
     * Filters Twenty Seventeen array of starter content.
     *
     * @since Twenty Seventeen 1.1
     *
     * @param array $starter_content Array of starter content.
     */
    $starter_content = apply_filters('twentyseventeen_starter_content', $starter_content);

    add_theme_support('starter-content', $starter_content);
}

add_action('after_setup_theme', 'twentyseventeen_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twentyseventeen_content_width() {

    $content_width = $GLOBALS['content_width'];

    // Get layout.
    $page_layout = get_theme_mod('page_layout');

    // Check if layout is one column.
    if ('one-column' === $page_layout) {
        if (twentyseventeen_is_frontpage()) {
            $content_width = 644;
        } elseif (is_page()) {
            $content_width = 740;
        }
    }

    // Check if is single post and there is no sidebar.
    if (is_single() && !is_active_sidebar('sidebar-1')) {
        $content_width = 740;
    }

    /**
     * Filter Twenty Seventeen content width of the theme.
     *
     * @since Twenty Seventeen 1.0
     *
     * @param $content_width integer
     */
    $GLOBALS['content_width'] = apply_filters('twentyseventeen_content_width', $content_width);
}

add_action('template_redirect', 'twentyseventeen_content_width', 0);

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
    $fonts_url = '';

    /**
     * Translators: If there are characters in your language that are not
     * supported by Libre Franklin, translate this to 'off'. Do not translate
     * into your own language.
     */
    $libre_franklin = _x('on', 'Libre Franklin font: on or off', 'twentyseventeen');

    if ('off' !== $libre_franklin) {
        $font_families = array();

        $font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

        $query_args = array(
            'family' => urlencode(implode('|', $font_families)),
            'subset' => urlencode('latin,latin-ext'),
        );

        $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');
    }

    return esc_url_raw($fonts_url);
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentyseventeen_resource_hints($urls, $relation_type) {
    if (wp_style_is('twentyseventeen-fonts', 'queue') && 'preconnect' === $relation_type) {
        $urls[] = array(
            'href' => 'https://fonts.gstatic.com',
            'crossorigin',
        );
    }

    return $urls;
}

add_filter('wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentyseventeen_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'twentyseventeen'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'twentyseventeen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Footer 1', 'twentyseventeen'),
        'id' => 'sidebar-2',
        'description' => __('Add widgets here to appear in your footer.', 'twentyseventeen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Footer 2', 'twentyseventeen'),
        'id' => 'sidebar-3',
        'description' => __('Add widgets here to appear in your footer.', 'twentyseventeen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'twentyseventeen_widgets_init');

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentyseventeen_excerpt_more($link) {
    if (is_admin()) {
        return $link;
    }

    $link = sprintf('<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>', esc_url(get_permalink(get_the_ID())),
            /* translators: %s: Name of current post */ sprintf(__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen'), get_the_title(get_the_ID()))
    );
    return ' &hellip; ' . $link;
}

add_filter('excerpt_more', 'twentyseventeen_excerpt_more');

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action('wp_head', 'twentyseventeen_javascript_detection', 0);

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function twentyseventeen_pingback_header() {
    if (is_singular() && pings_open()) {
        printf('<link rel="pingback" href="%s">' . "\n", get_bloginfo('pingback_url'));
    }
}

add_action('wp_head', 'twentyseventeen_pingback_header');

/**
 * Display custom color CSS.
 */
function twentyseventeen_colors_css_wrap() {
    if ('custom' !== get_theme_mod('colorscheme') && !is_customize_preview()) {
        return;
    }

    require_once( get_parent_theme_file_path('/inc/color-patterns.php') );
    $hue = absint(get_theme_mod('colorscheme_hue', 250));
    ?>
    <style type="text/css" id="custom-theme-colors" <?php
           if (is_customize_preview()) {
               echo 'data-hue="' . $hue . '"';
           }
           ?>>
               <?php echo twentyseventeen_custom_colors_css(); ?>
    </style>
    <?php
}

add_action('wp_head', 'twentyseventeen_colors_css_wrap');

/**
 * Enqueue scripts and styles.
 */
function twentyseventeen_scripts() {
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style('twentyseventeen-fonts', twentyseventeen_fonts_url(), array(), null);

    // Theme stylesheet.
    wp_enqueue_style('twentyseventeen-style', get_stylesheet_uri());

    // Load the dark colorscheme.
    if ('dark' === get_theme_mod('colorscheme', 'light') || is_customize_preview()) {
        wp_enqueue_style('twentyseventeen-colors-dark', get_theme_file_uri('/assets/css/colors-dark.css'), array('twentyseventeen-style'), '1.0');
    }

    // Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
    if (is_customize_preview()) {
        wp_enqueue_style('twentyseventeen-ie9', get_theme_file_uri('/assets/css/ie9.css'), array('twentyseventeen-style'), '1.0');
        wp_style_add_data('twentyseventeen-ie9', 'conditional', 'IE 9');
    }

    // Load the Internet Explorer 8 specific stylesheet.
    wp_enqueue_style('twentyseventeen-ie8', get_theme_file_uri('/assets/css/ie8.css'), array('twentyseventeen-style'), '1.0');
    wp_style_add_data('twentyseventeen-ie8', 'conditional', 'lt IE 9');

    // Load the html5 shiv.
    wp_enqueue_script('html5', get_theme_file_uri('/assets/js/html5.js'), array(), '3.7.3');
    wp_script_add_data('html5', 'conditional', 'lt IE 9');

    wp_enqueue_script('twentyseventeen-skip-link-focus-fix', get_theme_file_uri('/assets/js/skip-link-focus-fix.js'), array(), '1.0', true);

    $twentyseventeen_l10n = array(
        'quote' => twentyseventeen_get_svg(array('icon' => 'quote-right')),
    );

    if (has_nav_menu('top')) {
        wp_enqueue_script('twentyseventeen-navigation', get_theme_file_uri('/assets/js/navigation.js'), array('jquery'), '1.0', true);
        $twentyseventeen_l10n['expand'] = __('Expand child menu', 'twentyseventeen');
        $twentyseventeen_l10n['collapse'] = __('Collapse child menu', 'twentyseventeen');
        $twentyseventeen_l10n['icon'] = twentyseventeen_get_svg(array('icon' => 'angle-down', 'fallback' => true));
    }

    wp_enqueue_script('twentyseventeen-global', get_theme_file_uri('/assets/js/global.js'), array('jquery'), '1.0', true);

    wp_enqueue_script('jquery-scrollto', get_theme_file_uri('/assets/js/jquery.scrollTo.js'), array('jquery'), '2.1.2', true);

    wp_localize_script('twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'twentyseventeen_scripts');

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentyseventeen_content_image_sizes_attr($sizes, $size) {
    $width = $size[0];

    if (740 <= $width) {
        $sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
    }

    if (is_active_sidebar('sidebar-1') || is_archive() || is_search() || is_home() || is_page()) {
        if (!( is_page() && 'one-column' === get_theme_mod('page_options') ) && 767 <= $width) {
            $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
        }
    }

    return $sizes;
}

add_filter('wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2);

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function twentyseventeen_header_image_tag($html, $header, $attr) {
    if (isset($attr['sizes'])) {
        $html = str_replace($attr['sizes'], '100vw', $html);
    }
    return $html;
}

add_filter('get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3);

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function twentyseventeen_post_thumbnail_sizes_attr($attr, $attachment, $size) {
    if (is_archive() || is_search() || is_home()) {
        $attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
    } else {
        $attr['sizes'] = '100vw';
    }

    return $attr;
}

add_filter('wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3);

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function twentyseventeen_front_page_template($template) {
    return is_home() ? '' : $template;
}

add_filter('frontpage_template', 'twentyseventeen_front_page_template');

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path('/inc/custom-header.php');

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path('/inc/template-tags.php');

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path('/inc/template-functions.php');

/**
 * Customizer additions.
 */
require get_parent_theme_file_path('/inc/customizer.php');

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path('/inc/icon-functions.php');

function atg_menu_classes($classes, $item, $args) {
    if ($args->theme_location == 'Top Menu') {
        $classes[] = 'expanded dropdown';
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);
// start of woocomerce 
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main">';
}

function my_theme_wrapper_end() {
    echo '</section>';
}

add_action('after_setup_theme', 'woocommerce_support');

function woocommerce_support() {
    add_theme_support('woocommerce');
}

function drama_category_cpt() {
    $labels = array(
        'name' => _x('Drama', 'post type general name'),
        'singular_name' => _x('Drama', 'post type singular name'),
        'add_new' => _x('Add New', 'Drama'),
        'add_new_item' => __('Add New Drama'),
        'edit_item' => __('Edit Drama'),
        'new_item' => __('New Drama'),
        'all_items' => __('All Drama'),
        'view_item' => __('View Drama'),
        'search_items' => __('Search Drama'),
        'not_found' => __('No Drama found'),
        'not_found_in_trash' => __('No Drama found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Dramas'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Custom drama',
        'public' => true,
        'menu_position' => 30,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'has_archive' => true,
    );
    register_post_type('drama_category', $args);
}

add_action('init', 'drama_category_cpt');


// hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'drama_category_taxonomies');

// create two taxonomies, genres and writers for the post type "book"
function drama_category_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Drama Category', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Drama Category', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Drama Category', 'textdomain'),
        'all_items' => __('All Drama Categories', 'textdomain'),
        'parent_item' => __('Parent Drama Category', 'textdomain'),
        'parent_item_colon' => __('Parent Drama Category:', 'textdomain'),
        'edit_item' => __('Edit Drama Category', 'textdomain'),
        'update_item' => __('Update Drama Category', 'textdomain'),
        'add_new_item' => __('Add New Drama Category', 'textdomain'),
        'new_item_name' => __('New Drama Category Name', 'textdomain'),
        'menu_name' => __('Drama Category', 'textdomain'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'drama_category_detail')
    );

    register_taxonomy('drama_category_detail', array('drama_category'), $args);
}

function movies_category_cpt() {
    $labels = array(
        'name' => _x('Movie', 'post type general name'),
        'singular_name' => _x('Movie', 'post type singular name'),
        'add_new' => _x('Add New', 'Movie'),
        'add_new_item' => __('Add New Movie'),
        'edit_item' => __('Edit Movie'),
        'new_item' => __('New Movie'),
        'all_items' => __('All Movies'),
        'view_item' => __('View Movies'),
        'search_items' => __('Search Movies'),
        'not_found' => __('No Movie found'),
        'not_found_in_trash' => __('No Movie found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Movies'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Custom Movies',
        'public' => true,
        'menu_position' => 30,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt',),
        'has_archive' => true,
    );
    register_post_type('movies_category', $args);
}

add_action('init', 'movies_category_cpt');


// hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'movies_category_taxonomies');

// create two taxonomies, genres and writers for the post type "book"
function movies_category_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Movie Category', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Movie Category', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Movie Category', 'textdomain'),
        'all_items' => __('All Movie Category', 'textdomain'),
        'parent_item' => __('Parent Movie Category', 'textdomain'),
        'parent_item_colon' => __('Parent Movie Category:', 'textdomain'),
        'edit_item' => __('Edit Movie Category', 'textdomain'),
        'update_item' => __('Update Movie Category', 'textdomain'),
        'add_new_item' => __('Add New Movie Category', 'textdomain'),
        'new_item_name' => __('New Movie Category Name', 'textdomain'),
        'menu_name' => __('Movie Category', 'textdomain'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'movies_category_detail'),
    );

    register_taxonomy('movies_category_detail', array('movies_category'), $args);
}

function songs_catagory_cpt() {
    $labels = array(
        'name' => _x('Songs', 'post type general name'),
        'singular_name' => _x('Songs', 'post type singular name'),
        'add_new' => _x('Add New', 'Songs'),
        'add_new_item' => __('Add New Songs'),
        'edit_item' => __('Edit Songs'),
        'new_item' => __('New Songs'),
        'all_items' => __('All Songs'),
        'view_item' => __('View Songs'),
        'search_items' => __('Search Songs'),
        'not_found' => __('No Songs found'),
        'not_found_in_trash' => __('No Songs found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Songs'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Custom Songs',
        'public' => true,
        'menu_position' => 30,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => true,
    );
    register_post_type('songs_catagory', $args);
}

add_action('init', 'songs_catagory_cpt');

function anime_catagory_cpt() {
    $labels = array(
        'name' => _x('Anime', 'post type general name'),
        'singular_name' => _x('Songs', 'post type singular name'),
        'add_new' => _x('Add New', 'Songs'),
        'add_new_item' => __('Add New Songs'),
        'edit_item' => __('Edit Songs'),
        'new_item' => __('New Songs'),
        'all_items' => __('All Songs'),
        'view_item' => __('View Songs'),
        'search_items' => __('Search Songs'),
        'not_found' => __('No Songs found'),
        'not_found_in_trash' => __('No Songs found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Anime'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Anime',
        'public' => true,
        'menu_position' => 38,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'has_archive' => true,
    );
    register_post_type('anime_catagory', $args);
}

add_action('init', 'anime_catagory_cpt');

// function case_category_cpt() {
//     $labels = array(
//         'name' => _x('Case', 'post type general name'),
//         'singular_name' => _x('Case', 'post type singular name'),
//         'add_new' => _x('Add New', 'Case'),
//         'add_new_item' => __('Add New Case'),
//         'edit_item' => __('Edit Case'),
//         'new_item' => __('New Case'),
//         'all_items' => __('All Case'),
//         'view_item' => __('View Case'),
//         'search_items' => __('Search Case'),
//         'not_found' => __('No Case found'),
//         'not_found_in_trash' => __('No Case found in the Trash'),
//         'parent_item_colon' => '',
//         'menu_name' => 'Case'
//     );
//     $args = array(
//         'labels' => $labels,
//         'description' => 'Custom Movies',
//         'public' => true,
//         'menu_position' => 30,
//         'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
//         'has_archive' => true,
//     );
//     register_post_type('case_category', $args);
// }

// add_action('init', 'case_category_cpt');


// hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'case_category_taxonomies', 0);

// create two taxonomies, genres and writers for the post type "book"
function case_category_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x('Case Category', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Case Category', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Case Category', 'textdomain'),
        'all_items' => __('All Case Categories', 'textdomain'),
        'parent_item' => __('Parent Case Category', 'textdomain'),
        'parent_item_colon' => __('Parent Case Category:', 'textdomain'),
        'edit_item' => __('Edit Case Category', 'textdomain'),
        'update_item' => __('Update Case Category', 'textdomain'),
        'add_new_item' => __('Add New Case Category', 'textdomain'),
        'new_item_name' => __('New Case Category Name', 'textdomain'),
        'menu_name' => __('Case Category', 'textdomain'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'case_category_detail'),
    );

    register_taxonomy('case_category_detail', array('drama_category'), $args);

    // Add new taxonomy, NOT hierarchical (like tags)
    $labels = array(
        'name' => _x('Writers', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Writer', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Writers', 'textdomain'),
        'popular_items' => __('Popular Writers', 'textdomain'),
        'all_items' => __('All Writers', 'textdomain'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit Writer', 'textdomain'),
        'update_item' => __('Update Writer', 'textdomain'),
        'add_new_item' => __('Add New Writer', 'textdomain'),
        'new_item_name' => __('New Writer Name', 'textdomain'),
        'separate_items_with_commas' => __('Separate writers with commas', 'textdomain'),
        'add_or_remove_items' => __('Add or remove writers', 'textdomain'),
        'choose_from_most_used' => __('Choose from the most used writers', 'textdomain'),
        'not_found' => __('No writers found.', 'textdomain'),
        'menu_name' => __('Writers', 'textdomain'),
    );

    $args = array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'writer'),
    );

    register_taxonomy('writer', 'drama_category', $args);
}

function slider_cpt() {
    $labels = array(
        'name' => _x('Slider', 'post type general name'),
        'singular_name' => _x('Slider', 'post type singular name'),
        'add_new' => _x('Add New', 'Slider'),
        'add_new_item' => __('Add New Slider'),
        'edit_item' => __('Edit Slider'),
        'new_item' => __('New Slider'),
        'all_items' => __('All Slider'),
        'view_item' => __('View Slider'),
        'search_items' => __('Search Slider'),
        'not_found' => __('No Slider found'),
        'not_found_in_trash' => __('No Slider found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Slider'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Custom Slider',
        'public' => true,
        'menu_position' => 30,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => true,
    );
    register_post_type('slider', $args);
}

add_action('init', 'slider_cpt');

function new_episode_cpt() {
    $labels = array(
        'name' => _x('New-Episode', 'post type general name'),
        'singular_name' => _x('New-Episode', 'post type singular name'),
        'add_new' => _x('Add New', 'New-Episode'),
        'add_new_item' => __('Add New New-Episode'),
        'edit_item' => __('Edit New-Episode'),
        'new_item' => __('New New-Episode'),
        'all_items' => __('All New-Episode'),
        'view_item' => __('View New-Episode'),
        'search_items' => __('Search New-Episode'),
        'not_found' => __('No New-Episode found'),
        'not_found_in_trash' => __('No New-Episode found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'New Episode Slider'
        . ''
        . ''
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Custom Slider',
        'public' => true,
        'menu_position' => 30,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => true,
    );
    register_post_type('new_episode', $args);
}

add_action('init', 'new_episode_cpt');



require_once 'meta_boxes/init.php';

add_filter('cmb2_meta_boxes', 'ms_metaboxes');

function ms_metaboxes(array $meta_boxes) {

    $prefix = 'ms_';
    $meta_boxes['single_metaboxes'] = array(
        'id' => 'single_metaboxes',
        'title' => __('More Information', 'im'),
        'object_types' => array('', 'movies_category', 'songs_catagory', 'anime_catagory', 'case_category'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            // array(
            //     'name' => __('Include In Slider ', 'ms'),
            //     'desc' => __('', 'ms'),
            //     'id' => $prefix . 'slider_image',
            //     'type' => 'checkbox',
            // ),
            array(
                'name' => 'Heading',
                'desc' => '',
                'id' => $prefix . 'heading',
                'type' => 'text_medium'
            ),
            array(
                'name' => 'Text',
                'desc' => '',
                'id' => $prefix . 'detail',
                'type' => 'textarea'
            ),
            array(
                'name' => 'Name of the series',
                'desc' => '',
                'id' => $prefix . 'series-name',
                'type' => 'text'
            ),
            array(
                'name' => 'Name Bel',
                'desc' => '',
                'id' => $prefix . 'bel-name',
                'type' => 'text'
            ),
            array(
                'name' => 'Also known as the',
                'desc' => '',
                'id' => $prefix . 'also-known',
                'type' => 'text'
            ),
            array(
                'name' => 'number of rings',
                'desc' => '',
                'id' => $prefix . 'ring-number',
                'type' => 'text'
            ),
            array(
                'name' => 'Country Product',
                'desc' => '',
                'id' => $prefix . 'country',
                'type' => 'text'
            ),
            array(
                'name' => 'Date Broadcast',
                'desc' => '',
                'id' => $prefix . 'broadcast',
                'type' => 'text'
            ),
            
        ),
    );
   $meta_boxes['drama_metaboxes'] = array(
        'id' => 'single_metaboxes',
        'title' => __('More Information', 'im'),
        'object_types' => array('', 'drama_category'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => __('Best Dramas ', 'ms'),
                'desc' => __('', 'ms'),
                'id' => $prefix . 'first_img',
                'type' => 'checkbox',
            ),
             array(
                'name' => 'Heading',
                'desc' => '',
                'id' => $prefix . 'heading',
                'type' => 'text_medium'
            ),
            array(
                'name' => 'Text',
                'desc' => '',
                'id' => $prefix . 'detail',
                'type' => 'textarea'
            ),
            array(
                'name' => 'Name of the series',
                'desc' => '',
                'id' => $prefix . 'series-name',
                'type' => 'text'
            ),
            array(
                'name' => 'Name Bel',
                'desc' => '',
                'id' => $prefix . 'bel-name',
                'type' => 'text'
            ),
            array(
                'name' => 'Also known as the',
                'desc' => '',
                'id' => $prefix . 'also-known',
                'type' => 'text'
            ),
            array(
                'name' => 'number of rings',
                'desc' => '',
                'id' => $prefix . 'ring-number',
                'type' => 'text'
            ),
            array(
                'name' => 'Country Product',
                'desc' => '',
                'id' => $prefix . 'country',
                'type' => 'text'
            ),
            array(
                'name' => 'Date Broadcast',
                'desc' => '',
                'id' => $prefix . 'broadcast',
                'type' => 'text'
            ),
            
        ),
    ); 
    $meta_boxes['about-us_metaboxes'] = array(
        'id' => 'single_metaboxes',
        'title' => __('More Information', 'im'),
        'object_types' => array('', 'about_us'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Designation',
                'desc' => '',
                'id' => $prefix . 'designation',
                'type' => 'text_small'
            ),
            array(
                'name' => 'Text',
                'desc' => '',
                'id' => $prefix . 'text',
                'type' => 'text'
            ),
            array(
                'name' => 'Email',
                'id' => $prifix . 'email',
                'type' => 'email',
            ),
            array(
                'name' => 'Instagram',
                'id' => $prefix . 'instagram',
                'type' => 'text_url',
            ),
            array(
                'name' => 'Tweeter',
                'desc' => '',
                'id' => $prefix . 'tweeter',
                'type' => 'text_url',
            ),
            array(
                'name' => 'Facebook',
                'desc' => '',
                'id' => $prefix . 'facebook',
                'type' => 'text_url',
            ),
        ),
    );

    $meta_boxes['single_metaboxes1'] = array(
        'id' => 'single_metaboxes1',
        'title' => __('More Information', 'im'),
        'object_types' => array('', 'slider'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'URL',
                'desc' => '',
                'id' => $prefix . 'slider-url-1',
                'type' => 'text_url',
            ),
            array(
                'name' => 'Upload Image',
                'desc' => '',
                'id' => $prefix . 'slider1',
                'type' => 'file',
                'allow' => array('url', 'attachment') // limit to just attachments with array( 'attachment' )
            ),
            array(
                'name' => 'URL',
                'desc' => '',
                'id' => $prefix . 'slider-url-2',
                'type' => 'text_url',
            ),
            array(
                'name' => 'Upload Image',
                'desc' => '',
                'id' => $prefix . 'slider2',
                'type' => 'file',
                'allow' => array('url', 'attachment') // limit to just attachments with array( 'attachment' )
            ),
        ),
    );



    return $meta_boxes;
}

function about_us_cpt() {
    $labels = array(
        'name' => _x('about_us', 'post type general name'),
        'singular_name' => _x('About-us', 'post type singular name'),
        'add_new' => _x('Add New', 'About-us'),
        'add_new_item' => __('Add New About-us'),
        'edit_item' => __('Edit About-us'),
        'new_item' => __('New About-us'),
        'all_items' => __('All About-us'),
        'view_item' => __('View About-us'),
        'search_items' => __('Search About-us'),
        'not_found' => __('No About-us found'),
        'not_found_in_trash' => __('No About-us found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Our-Team'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'About-us',
        'public' => true,
        'menu_position' => 30,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => true,
    );
    register_post_type('about_us', $args);
}

add_action('init', 'about_us_cpt');


//Register widget

function arphabet_widgets_init() {

    register_sidebar(array(
        'name' => 'Home right sidebar',
        'id' => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'arphabet_widgets_init');

function singlepage_permalink() {

    $url = 'localhost/hopesubs/drama_category/love-returns/?epi=2';

    $values = parse_url($url);

    $host = explode('.', $values['host']);
}

function strbefore($string, $substring) {
    $pos = strpos($string, $substring);
    if ($pos === false)
        return $string;
    else
        return(substr($string, 0, $pos));
}
