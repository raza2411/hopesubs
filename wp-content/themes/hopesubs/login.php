<?php
/**
 * Template Name: Login Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */




if($_POST) {
 
    global $wpdb;
 
    //We shall SQL escape all inputs
    $username = $wpdb->escape($_REQUEST['username']);
    $password = $wpdb->escape($_REQUEST['password']);
    $remember = $wpdb->escape($_REQUEST['rememberme']);
 
    if($remember) $remember = "true";
    else $remember = "false";
 
    $login_data = array();
    $login_data['user_login'] = $username;
    $login_data['user_password'] = $password;
    $login_data['remember'] = $remember;
 
    $user_verify = wp_signon( $login_data, false ); 
 
    if ( is_wp_error($user_verify) ) 
    {
       header("Location: " . home_url() . "/login/error/");
       // Note, I have created a page called "Error" that is a child of the login page to handle errors. This can be anything, but it seemed a good way to me to handle errors.
     } else {   
       echo "<script type='text/javascript'>window.location='". home_url() ."'</script>";
       exit();
     }
 
} else {
 
    // No login details entered - you should probably add some more user feedback here, but this does the bare minimum
 
    echo "Invalid login details";
 
}

    ?>
<div class="modal fade" id="logIn" tabindex="-1" role="dialog" aria-labelledby="logInTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="logInTitle">تسجيل الدخول</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="logInInputEmail">البريد الإلكتروني</label>
                                    <input type="email" class="form-control" id="logInInputEmail" aria-describedby="emailHelp" placeholder="ادخل بريدك الإلكترونى"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label for="logInInputPassword">كلمة المرور</label>
                                    <input type="password" class="form-control" id="logInInputPassword" placeholder="كلمة المرور" required>
                                </div>
                                <button type="submit" class="btn">تسجيل دخول</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                        </div>
                    </div>
                </div>
            </div>

