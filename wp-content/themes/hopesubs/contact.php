<?php

/**
 * Template Name: Contact Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
                <?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();
                      
                        ?>
<section class="form">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12">
                
                <img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" alt="#">
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">

               <?php the_content(); ?>
               
            </div>
        </div>
    </div>
</section>
<?php
 endwhile;
                endif;
			?>
<?php

get_footer();
